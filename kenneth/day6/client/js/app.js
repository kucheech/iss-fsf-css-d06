/**
 * Client side code.
 */
(function () {
    var app = angular.module("RegApp", []);

    app.controller("RegistrationCtrl", ["$http", RegistrationCtrl]);

    function RegistrationCtrl($http) {
        var self = this; // vm

        self.user = {
            email: "",
            password: "",
            confirmPassword: "",
            gender: "",
            dob: ""
        };

        self.displayUser = {
            email: "",
            password: "",
            confirmPassword: ""
        };

        self.isFemale = function () {
            return self.user.gender == "F";
        }

        self.isAbove18 = function () {
            if (self.user.dob == "") {
                return false;
            }

            return _calculateAge(self.user.dob) > 18;
        }

        self.registerUser = function () {
            // console.log(self.user.email);
            // console.log(self.user.password);
            $http.post("/users", self.user)
                .then(function (result) {
                    console.log(result);
                    self.displayUser.email = result.data.email;
                    self.displayUser.password = result.data.password;
                }).catch(function (e) {
                    console.log(e);
                });
        };
    }

    function _calculateAge(birthday) { // birthday is a date
        var ageDifMs = Date.now() - birthday.getTime();
        var ageDate = new Date(ageDifMs); // miliseconds from epoch
        return Math.abs(ageDate.getUTCFullYear() - 1970);
    }
})();