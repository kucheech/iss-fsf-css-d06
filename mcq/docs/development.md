## Create project
1. Create a project with server and client folders & files
2. Install express with ```npm install express --save```
3. Add and configure a .bowerrc
4. Install angular with ```bower install angular```

## Setup server at server/app.js
1. setup GET endpoints for "/", "/questions" and POST endpoint for "/answer"
1. "/" will return client/index.html
1. "/questions" will return the first question from a shuffled array
1. "/post" will check if the submitted answer is correct
1. create a data structure for the questions and populate with some data
Example JSON data structure:
```{
    id: 0,
   text: "What is your favourite colour?",
    options: ["red", "green", "blue", "yellow", "purple"],
    answer: 0
}```
1. create an array of questions
1. Add a function to shuffle this array of questions

## Setup client at client/js/app.js
1. create angularjs module QuizApp with controller QuizCtrl
1. create a constructor which will do a GET /questions to retrieve one question and display it
1. at index.html, use ```ng-app="QuizApp```
1. and ```ng-controller="QuizCtrl as ctrl"```
1. in the form, use ```ng-submit="ctrl.submitAnswer()```
1. use ng-repeat to display the 5 options, with ```ng-value="{{$index}}"```
1. do a /answer post upon submit in which the server will check the answer submitted
1. display "Correct" or "Wrong" accordingly from the response of the above post

## Start server/app.js
1. ```nodemon```

## Start client at browser
1. ```http://localhost:3000```